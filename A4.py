import json
import gzip
import urllib.request
from pymongo import MongoClient

#key and auth are needed because we add them at the end of the URL to have 10000 request per day
key = urllib.request.quote("1YkNbcuJ58eMUCdFxKOvBA((")
auth = '&key='+key
URL = "http://api.stackexchange.com/2.2/posts/%s/comments?page=%d&order=desc&sort=creation&site=stackoverflow"

#To connect to the mongo server
client = MongoClient()
db = client.db

#Function that enables us to download data and convert them in a Python dictionary
def urlretrieve(url):
    with urllib.request.urlopen(url+auth) as f:
        return json.loads(gzip.GzipFile(fileobj=f).read().decode('utf-8'))

#We obtain from our database both the "question_id" and the "answer_id"
def getids(questions, answers):
    ids = []
    for question in questions.find():
        ids.append(question.get("question_id"))
    for answer in answers.find():
        ids.append(answer.get("answer_id"))
    return ids


if __name__ == '__main__':
    p=1
    ids = getids(db.questions, db.answers)
    #In order not to use too many requests we groups of a hundred of ids
    for k in range(0, len(ids), 100):
        str_ids = ";".join(str(id) for id in ids[k:k+100])
        url = URL % (str_ids, p)
        data = urlretrieve(url)
        if data["items"]:
            db.comments.insert_many(data["items"])
        if not data or not data["has_more"]:
            p=1
        else:
            p+=1