import gzip
import json
import urllib.request
from pymongo import MongoClient

#List of different tags so we can change later if we do not have the requested quantity of questions with just one tag
tags = ["mongodb", "javascript", "java", "c#", "php", "android"]

#key and auth are needed because we add them at the end of the URL to have 10000 request per day
key = urllib.request.quote("1YkNbcuJ58eMUCdFxKOvBA((")
auth = '&key='+key
URL = 'https://api.stackexchange.com/2.2/questions?tagged=%s&filter=withbody&page=%d&pagesize=100&order=asc&sort=creation&site=stackoverflow'

#To connect to the mongo server
client = MongoClient()
db = client.db

#Function that enables us to download data and convert them in a Python dictionary
def urlretrieve(url):
    with urllib.request.urlopen(url+auth) as f:
        return json.loads(gzip.GzipFile(fileobj=f).read().decode('utf-8'))


if __name__ == '__main__':
    j = 0
    p = 1
    #We download 100 questions per request/page so we run the cycle 100 times in order to obtain 10000 questions
    for i in range(0, 100):
        print(i)
        url = URL % (tags[j], p)
        data = urlretrieve(url)
        db.questions.insert_many(data["items"])
        if not data["items"] or not data["has_more"]:
            j=j+1
            p = 1
        else:
            p=p+1