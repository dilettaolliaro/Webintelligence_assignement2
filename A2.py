import json
import gzip
import urllib.request
from pymongo import MongoClient

#key and auth are needed because we add them at the end of the URL to have 10000 request per day
key = urllib.request.quote("1YkNbcuJ58eMUCdFxKOvBA((")
auth = '&key='+key
URL = "http://api.stackexchange.com/2.2/users/%s?pagesize=100&site=stackoverflow"

#To connect to the mongo server
client = MongoClient()
db = client.db

#Function that enables us to download data and convert them in a Python dictionary
def urlretrieve(url):
    with urllib.request.urlopen(url+auth) as f:
        return json.loads(gzip.GzipFile(fileobj=f).read().decode('utf-8'))

#We create a list of user ids then we use the function set() in order to avoid duplicates
def getidsusers(questions):
    ids = []
    for question in questions.find():
        if question.get("owner").get("user_id"):
                ids.append(question.get("owner").get("user_id"))
    return list(set(ids))


if __name__ == '__main__':
    ids = getidsusers(db.questions)
    #In order not to use too many requests we groups of a hundred of ids
    for k in range(0, len(ids), 100):
        str_ids = ";".join(str(id) for id in ids[k:k+100])
        url = URL % str_ids
        data = urlretrieve(url)
        db.users.insert_many(data["items"])

