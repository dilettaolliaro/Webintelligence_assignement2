from pymongo import MongoClient

#To connect to the mongo server
client = MongoClient()
db = client.db

#These are the query we are going to use in order to obtain the 10 users that wrote more answers/questions/comments
queryforanswers = db.answers.aggregate([{"$match": {"owner.user_id": {"$nin": [None]}}},{"$group" : {"_id": "$owner.user_id", "count": {"$sum":1}}}, {"$sort": {"count":-1}}, {"$limit": 10}])
queryforquestions = db.questions.aggregate([{"$match": {"owner.user_id": {"$nin": [None]}}},{"$group" : {"_id": "$owner.user_id", "count": {"$sum":1}}}, {"$sort": {"count":-1}}, {"$limit": 10}])
queryforcomments = db.comments.aggregate([{"$match": {"owner.user_id": {"$nin": [None]}}},{"$group" : {"_id": "$owner.user_id", "count": {"$sum":1}}}, {"$sort": {"count":-1}}, {"$limit": 10}])

#We store the results of our queries in a file, so that later it would be easier to retrieve them
def gettoptensusers(query, id):
    rank=1
    with open("TopTenUsers" + id + ".txt", "w") as f:
        for user in query:
            f.write("%d, User_id: %s, Counter: %d\n" % (rank, user["_id"], user["count"]))
            rank+=1


if __name__ == '__main__':
    gettoptensusers(queryforquestions, "Questions")
    gettoptensusers(queryforanswers, "Answers")
    gettoptensusers(queryforcomments, "Comments")