import gensim
from pymongo import MongoClient

#To connect to the mongo server
client = MongoClient()
db = client.db

def create_lems():
    listoflems = []
    listofusers = []
    for user in db.users.find():
        lems = []
        for word in user.get("words"):
            lems.extend(gensim.utils.lemmatize(word["word"])*word["count"])
        listofusers.append((user.get("_id"),user.get("user_id")))
        listoflems.append(lems)
    return listoflems, listofusers

#We create our lexicon
def create_lexicon(listoflems):
    lexicon = gensim.corpora.Dictionary(listoflems)
    lexicon.filter_extremes(no_below=2)
    return lexicon


# We represent our list of lems with the bag of words representation
def create_bow(lexicon, listoflems):
    bows = [lexicon.doc2bow(n) for n in listoflems]
    return bows


# We create tfidf model based on the bows created before
def do_tfidf(bows):
    tfidf = gensim.models.TfidfModel(bows)
    return tfidf


# This function implements the cosine similarity
def do_cossim(model, user, all_bows):
    sim = [(doc_id, gensim.matutils.cossim(user, model[all_bows[doc_id]])) for doc_id in range(len(all_bows))]
    return sorted(sim, key=lambda score: score[1], reverse=True)

#After applying the cosine similarity to every user we add the attribute "words" to every document "user"
if __name__ == '__main__':
    listoflems, listofusers = create_lems()
    lexicon = create_lexicon(listoflems)
    all_bows = create_bow(lexicon, listoflems)
    tfidf = do_tfidf(all_bows)
    for i in range(len(listofusers)):
        result = do_cossim(tfidf, tfidf[all_bows[i]], all_bows)
        result = [{"user_id": listofusers[user_i][1], "score": score} for user_i, score in result if user_i != i]
        db.users.update({'_id': listofusers[i][0]},
                        {'$set': {'similar_users': result[:10]}})