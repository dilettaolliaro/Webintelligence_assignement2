import re
from bs4 import BeautifulSoup
from pymongo import MongoClient
import io

#This is a regexp in order to avoid URLs strings in our texts, it was taken from @diegoperini's solution at https://mathiasbynens.be/demo/url-regex
urlregexp = re.compile(r"_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x00a1-\xffff0-9]+-?)*[a-z\x00a1-\xffff0-9]+)(?:\.(?:[a-z\x00a1-\xffff0-9]+-?)*[a-z\x00a1-\xffff0-9]+)*(?:\.(?:[a-z\x00a1-\xffff]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$_iuS")

#We save in a global list, the list of stopwords for convenience
stopwords = []
with io.open("stopwords-en.txt", "r", encoding="utf8") as f:
    for word in f:
        stopwords.append(word.strip())

#To connect to the mongo server
client = MongoClient()
db = client.db


#We count the occurences of every (significant) word
def getfrequencies(text):
    freq={}
    for word in re.split("[^a-z']+", text.lower()):
        if word and word not in stopwords:
            if word not in freq:
                freq[word] = 0
            freq[word] += 1
    return freq

#We use BeautifulSoup to extract from the whole HTML text with tags and other non-significant elements the interesting text for us,
#moreover we avoid to consider the words contained between <code> and </code> and the ones contained in urls
def parse(text):
    result = BeautifulSoup(text, "lxml")
    for code in result.find_all("code"):
        code.extract()
    result = result.get_text()
    return urlregexp.sub("", result)

#We sort our result, taking just the first 50 words for every user, and then we format it as we want
def gettopfiftywords(user):
    words = ""
    for question in db.questions.find({"owner.user_id": user}):
        if question.get("body"):
            words = words + ' ' + question.get("body")
    for answer in db.answers.find({"owner.user_id": user}):
        if answer.get("body"):
            words = words + ' ' + answer.get("body")
    for comment in db.comments.find({"owner.user_id": user}):
        if comment.get("body"):
            words = words + ' ' + comment.get("body")
    text = parse(words)
    frequencies = getfrequencies(text)
    sortedfreq = sorted(frequencies.items(), key=lambda k: k[1], reverse=True)[:50]
    return [{"word":word,"count":count}for word,count in sortedfreq]

#We add the attribute "words" to every document "user"
if __name__ == '__main__':
    for user in db.users.find():
        db.users.update({'_id': user.get("_id")},
                             {'$set': {'words': gettopfiftywords(user.get("user_id"))}})
